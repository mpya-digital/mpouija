# Mpouija

Ouija board game created during the Halloween hackathon 2019

## How to run the game

Make sure you have installed the required dependencies in both the client and server directories using `yarn install`

To run the client you need to navigate inside /client and execute `yarn start`

To run the server you need to navigate inside /server and execute `yarn watch`

## Pages

The app consists of 5 pages:

* Welcome
* Lobby
* Game
* Leaderboard
* Admin

### Welcome

From this screen new players can join the game

### Lobby

This is the waiting room before a game begins where you can see the other players that have joined

### Game

The main game screen. This is where you can see the ouija board and submit guesses for the word

### Leaderboard

This screen is displayed to the players between rounds. It contains a list of all players and their points

### Admin

The admin screen is where all the magic happens. Here we have four buttons for controlling the game:

* Start round - the server selects a new word and displays the game board to the players

* End round - the server displays the leaderboard to the players

* Reset game - kick out all the players

* Give hint - will set the first and last characters of the word to display correctly and shuffle the characters in between
