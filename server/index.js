const Koa = require('koa')
const socket = require('socket.io')
const http = require('http')

const app = new Koa()

const server = http.createServer(app.callback())
const io = socket(server)

let users = {}

let possibleWords = require('./words').words
const INTERVAL = 3000
let currentWord = ''
let roundStartedAt = 0

const getPoints = () => {
  const maxPoints = currentWord.length * 10
  const totalSecondsElapsed = (Date.now() - roundStartedAt) / 1000
  const loadingTime = (INTERVAL * (currentWord.length - 1)) / 1000
  if (totalSecondsElapsed < loadingTime) {
    return maxPoints
  }
  return Math.max(
    Math.floor(maxPoints - (totalSecondsElapsed - loadingTime)),
    0
  )
}

const getLeaderBoard = () => {
  return Object.keys(users)
    .map(id => users[id])
    .sort((a, b) => b.points - a.points)
}

io.on('connection', function (socket) {
  console.log('Connection made!')

  socket.on('Join', name => {
    console.log(name, 'joined the game')
    users[socket.id] = { name, points: 0 }
    socket.broadcast.emit('UpdateLeaderboard', getLeaderBoard())
  })

  socket.on('GetLeaderBoard', (_, cb) => {
    console.log('users', users)
    cb(getLeaderBoard())
    console.log('Get leaderboard')
  })

  socket.on('StartRound', () => {
    socket.broadcast.emit('RoundStarted')
    const word = startRound(socket)
    socket.emit('ChosenWord', word)
    console.log('Start round')
  })

  socket.on('EndRound', () => {
    socket.emit('UpdateLeaderboard', getLeaderBoard())
    socket.broadcast.emit('RoundEnded', getLeaderBoard())
    console.log('End round')
  })

  socket.on('ResetGame', () => {
    users = {}
    socket.emit('UpdateLeaderboard', getLeaderBoard())
    socket.broadcast.emit('ResetGame')
    console.log('Reset game')
  })

  socket.on('Guess', (word, cb) => {
    console.log('Guess')
    const correct = word.toLowerCase() === currentWord.toLowerCase()

    if (correct && users[socket.id]) {
      let points = getPoints()
      users[socket.id].points += points
      socket.broadcast.emit('UpdateLeaderboard', getLeaderBoard())
      console.log('Correct guess, points awarded:', points)
    }

    cb({ correct })
  })

  socket.on('Hint', () => {
    console.log('Hint')
    const first = currentWord.charAt(0)
    const last = currentWord.charAt(currentWord.length - 1)
    const rest = currentWord.substring(1, currentWord.length - 1)
    const shuffled = getShuffledArray(rest)
    socket.broadcast.emit('ReceiveHint', first + shuffled.join('') + last)
  })

  socket.on('disconnect', () => {
    console.log('user disconnected')
  })
})

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[a[i], a[j]] = [a[j], a[i]]
  }
  return a
}

const getShuffledArray = currentWord => {
  let shuffledArray = shuffle(currentWord.split(''))
  while (shuffledArray[0] === currentWord.charAt(0)) {
    shuffledArray = shuffle(currentWord.split(''))
  }
  return shuffledArray
}

const startRound = socket => {
  socket.broadcast.emit('RoundStarted')
  roundStartedAt = new Date()
  const wordIndex = Math.floor(Math.random() * possibleWords.length)
  currentWord = possibleWords[wordIndex]
  possibleWords.splice(wordIndex, 1)
  const shuffledArray = getShuffledArray(currentWord)

  for (let i = 0; i < shuffledArray.length; i++) {
    setTimeout(() => {
      socket.broadcast.emit('DisplayLetter', { letter: shuffledArray[i] })
    }, i * INTERVAL)
  }

  return currentWord
}

server.listen(3001)

process.on('warning', e => console.warn(e.stack))
