import React from 'react'
import ReactDOM from 'react-dom'
import 'semantic-ui-css/semantic.min.css'
import App from './components/App'
import './globals.css'

ReactDOM.render(<App />, document.getElementById('root'))
