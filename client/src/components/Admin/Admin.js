import React, { useState, useEffect } from 'react'
import { Button, Grid, Segment, Message } from 'semantic-ui-react'
import Leaderboard from '../Leaderboard'

const Admin = props => {
  const [chosenWord, setChosenWord] = useState('')
  const { socket } = props

  useEffect(() => {
    socket.on('ChosenWord', data => {
      setChosenWord(data)
    })

    return () => {
      socket.off('ChosenWord')
    }
  }, [socket])

  const resetGame = () => {
    socket.emit('ResetGame')
  }

  const startRound = () => {
    socket.emit('StartRound')
  }

  const endRound = () => {
    socket.emit('EndRound')
  }

  const hint = () => {
    socket.emit('Hint')
  }

  return (
    <Grid doubling centered columns={2} style={{ width: '100%' }}>
      <Grid.Column textAlign="center">
        <Segment>
          <Button onClick={startRound} color="green">
            Start round
          </Button>
          <Button onClick={endRound} color="red">
            End round
          </Button>
          <Button onClick={resetGame} color="black">
            Reset game
          </Button>
          <Button onClick={hint} color="blue">
            Give hint
          </Button>
        </Segment>
        {chosenWord && <Message>The chosen word is: {chosenWord}</Message>}
        <Leaderboard socket={socket} />
      </Grid.Column>
    </Grid>
  )
}

export default Admin
