import React, { useEffect, useState } from 'react'
import history from '../../history'

const Lobby = props => {
  const [userList, setUserList] = useState([])
  const { socket } = props

  useEffect(() => {
    socket.emit('GetLeaderBoard', null, data => {
      setUserList(data)
    })

    socket.on('RoundStarted', () => {
      history.push('/game')
    })

    socket.on('UpdateLeaderboard', data => {
      setUserList(data)
    })

    return () => {
      socket.off('GetLeaderBoard')
      socket.off('RoundStarted')
      socket.off('UpdateLeaderboard')
    }
  }, [socket])

  const renderUserList = () => {
    return userList.map(({ name }, idx) => {
      return <div key={idx}>{name}</div>
    })
  }
  return (
    <div>
      <p>Waiting for users to joyn...</p>
      <div>{renderUserList()}</div>
    </div>
  )
}

export default Lobby
