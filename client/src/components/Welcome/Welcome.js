import React, { useState } from 'react'
import { Button } from 'semantic-ui-react'
import history from '../../history'

const Welcome = props => {
  const [name, setName] = useState('')
  const socket = props.socket

  const handleKeyPress = event => {
    if (event.key === 'Enter') {
      buttonClick()
    }
  }

  const buttonClick = () => {
    if (name.length > 0) {
      socket.emit('Join', name)
      history.push('/lobby')
    }
  }
  return (
    <div>
      <input
        id="nameInput"
        type="text"
        onInput={e => setName(e.target.value)}
        onKeyDown={handleKeyPress}
      ></input>
      <Button color="green" style={{ marginLeft: 10 }} onClick={buttonClick}>
        JOYN
      </Button>
    </div>
  )
}

export default Welcome
