import React from 'react'
import Board from './Board'
import Answer from './Answer'
import styles from './Game.module.css'

const Game = props => {
  return (
    <div className={styles.game}>
      <Board socket={props.socket} />
      <Answer socket={props.socket} />
    </div>
  )
}

export default Game
