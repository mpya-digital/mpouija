import React, { useState, useEffect } from 'react'
import { Button } from 'semantic-ui-react'
import styles from './Answer.module.css'

const Answer = props => {
  const [guess, setGuess] = useState('')
  const [correctGuess, setCorrectGuess] = useState(false)
  const [inCorrectGuess, setInCorrectGuess] = useState(false)
  const { socket } = props

  useEffect(() => {
    socket.on('RoundStarted', () => {
      setCorrectGuess(false)
      setInCorrectGuess(false)
    })

    return () => {
      socket.off('RoundStarted')
    }
  }, [socket])

  const handleKeyPress = event => {
    if (event.key === 'Enter') {
      buttonClick()
    }
  }

  const buttonClick = () => {
    if (guess.length > 0) {
      socket.emit('Guess', guess, response => {
        if (response.correct === true) {
          setCorrectGuess(true)
        } else {
          setInCorrectGuess(true)
        }
      })
    }
  }

  const clearGuess = () => {
    setGuess('')
    document.getElementById('guessInput').value = ''
  }

  return (
    <div className={styles.answer}>
      {!correctGuess && (
        <>
          <div className={styles.row}>
            <Button
              style={{ marginRight: '10px' }}
              color="red"
              onClick={clearGuess}
            >
              Clear
            </Button>
            <input
              id="guessInput"
              type="text"
              autoComplete="off"
              onInput={e => setGuess(e.target.value)}
              onKeyDown={handleKeyPress}
            ></input>
            <Button
              style={{ marginLeft: '10px' }}
              color="green"
              onClick={buttonClick}
            >
              Guess
            </Button>
          </div>
          {inCorrectGuess && (
            <div className={styles.text}>
              <span>Horribly wrong! Try again...</span>
            </div>
          )}
        </>
      )}
      {correctGuess && (
        <div className={styles.text}>
          <span>Stunningly right!</span>
        </div>
      )}
    </div>
  )
}

export default Answer
