import React, { useEffect, useState } from 'react'
import history from '../../../history'
import styles from './Board.module.css'
import { alphabet } from './Board.constants'

const Board = props => {
  const socket = props.socket
  const [displayLetter, setDisplayLetter] = useState('')
  const [receivedLetters, setReceivedLetters] = useState([])

  useEffect(() => {
    socket.on('RoundEnded', () => {
      history.push('/leaderboard')
    })

    socket.on('RoundStarted', () => {
      setReceivedLetters([])
    })

    socket.on('ReceiveHint', data => {
      setReceivedLetters(data.split(''))
    })

    socket.on('ResetGame', () => {
      history.push('/')
    })

    return () => {
      socket.off('RoundEnded')
      socket.off('RoundStarted')
      socket.off('ReceiveHint')
      socket.off('ResetGame')
    }
  }, [socket])

  useEffect(() => {
    socket.on('DisplayLetter', data => {
      setDisplayLetter(data.letter)
      setReceivedLetters([...receivedLetters, data.letter])
    })

    return () => {
      socket.off('DisplayLetter')
    }
  }, [receivedLetters, socket])

  const renderCharacters = (start, end) => {
    return alphabet.slice(start, end).map((character, idx) => {
      if (character === displayLetter) {
        return (
          <tspan key={idx} className={styles.glow}>
            {character}
          </tspan>
        )
      }
      return <tspan key={idx}>{character}</tspan>
    })
  }

  const renderReceivedLetters = () => {
    return receivedLetters.map((letter, idx) => {
      return <span key={idx}>{letter}</span>
    })
  }

  return (
    <div className={styles.board}>
      <svg
        viewBox="-20 -50 880 320"
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
      >
        <path
          id="curve1"
          d="M0.29,128.016c0,0,179.394-127.516,424.091-127.516
	          c253.03,0,434.091,126.516,434.091,126.516"
        />
        <path
          id="curve2"
          d="M30.29,264.49c0,0,149.394-147.516,394.091-147.516
            c253.03,0,414.091,146.516,414.091,146.516"
        />
        <text>
          <textPath xlinkHref="#curve1">{renderCharacters(0, 13)}</textPath>
        </text>
        <text>
          <textPath xlinkHref="#curve2">{renderCharacters(13)}</textPath>
        </text>
      </svg>
      <div className={styles.receivedLetters}>{renderReceivedLetters()}</div>
    </div>
  )
}

export default Board
