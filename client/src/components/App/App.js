import React from 'react'
import socketIO from 'socket.io-client'
import Game from '../Game'
import Admin from '../Admin'
import Leaderboard from '../Leaderboard'
import Lobby from '../Lobby'
import Welcome from '../Welcome'
import { Router, Route, Switch } from 'react-router-dom'
import history from '../../history.js'
import styles from './App.module.css'

const App = () => {
  // const socket = socketIO('http://192.168.86.99:3001')
  const socket = socketIO('localhost:3001')

  return (
    <div>
      <Router history={history}>
        <div className={styles.app}>
          <h1>MPOUIJA</h1>
          <Switch>
            <Route path="/" exact render={() => <Welcome socket={socket} />} />
            <Route path="/game" exact render={() => <Game socket={socket} />} />
            <Route
              path="/leaderboard"
              exact
              render={() => <Leaderboard socket={socket} />}
            />
            <Route
              path="/lobby"
              exact
              render={() => <Lobby socket={socket} />}
            />
            <Route
              path="/admin"
              exact
              render={() => <Admin socket={socket} />}
            />
          </Switch>
        </div>
      </Router>
    </div>
  )
}

export default App
