import React, { useEffect, useState } from 'react'
import { Grid, Header, Segment } from 'semantic-ui-react'
import history from '../../history'

const Leaderboard = props => {
  const { socket } = props
  const [leaderBoard, setLeaderBoard] = useState([])
  useEffect(() => {
    socket.emit('GetLeaderBoard', null, data => {
      setLeaderBoard(data)
    })

    socket.on('UpdateLeaderboard', data => {
      setLeaderBoard(data)
    })

    socket.on('RoundStarted', () => {
      history.push('/game')
    })

    socket.on('ResetGame', () => {
      history.push('/')
    })

    return () => {
      socket.off('GetLeaderBoard')
      socket.off('UpdateLeaderboard')
      socket.off('RoundStarted')
      socket.off('ResetGame')
    }
  }, [socket])

  const renderLeaderBoard = () => {
    return leaderBoard.map(({ name, points }, idx) => {
      return (
        <Grid.Row style={{ color: 'black' }} key={idx} columns={2}>
          <Grid.Column>{name}</Grid.Column>
          <Grid.Column>{points}</Grid.Column>
        </Grid.Row>
      )
    })
  }

  return (
    <Grid doubling centered columns={2} style={{ width: '100%' }}>
      <Grid.Column textAlign="center">
        <Segment style={{ paddingBottom: 30 }}>
          <Header as="h2" style={{ marginBottom: 30 }}>
            Leaderboard
          </Header>
          <Grid divided="vertically">{renderLeaderBoard()}</Grid>
        </Segment>
      </Grid.Column>
    </Grid>
  )
}

export default Leaderboard
